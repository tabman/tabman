import "reflect-metadata"
import { DataSource } from "typeorm"
import path from "path"

export const dbConnection = new DataSource({
    type: "sqlite",
    database: path.resolve(__dirname, "..", "sqlite.db"),
    synchronize: true,
    logging: false,
    entities: [],
    migrations: [],
    subscribers: [],
})
